[![pipeline status](https://gitlab.com/nicosingh/rpi-openvpn-server/badges/master/pipeline.svg)](https://gitlab.com/nicosingh/rpi-openvpn-server/commits/master) [![Docker Pulls](https://img.shields.io/docker/pulls/nicosingh/rpi-openvpn-server.svg)](https://hub.docker.com/r/nicosingh/rpi-openvpn-server/)

# About

Docker image of Raspbian with OpenVPN server already installed.

**IMPORTANT**: This image excludes the creation of server/client Keys. This OpenVPN server uses some keys which are externally created, for instance using [rpi-openvpn-keygenerator](https://hub.docker.com/r/nicosingh/rpi-openvpn-keygenerator/) image.

# How to use this Docker image?

We can start our own OpenVPN server using something like:

`docker run -d -p your_port:1194/udp -v my_local_keys:/etc/openvpn/easy-rsa/keys --privileged -e OVPN_LOCAL_NETWORK=192.168.0.0 nicosingh/rpi-openvpn-server`

Where:

`docker run -d`: means to run a new Docker container as a daemon

`-p your_port:1194/udp`: will forward internal 1194 port from the container to your_port port (change it as you wish). Remember to use UDP forwarding, OpenVPN server is configured to use UDP!.

`my_local_keys:/etc/openvpn/easy-rsa/keys`: will be used to map our server and client keys (externally generated) to our OpenVPN server, from our local `my_local_keys` folder.

`--privileged`: is required because some part of this image configures IPTables rules, that require root access.

`-e OVPN_LOCAL_NETWORK=192.168.0.0`: is an environment variable to define our RaspberryPi's local network. The real one, from our router, i.e. 192.168.0.0. **IMPORTANT**: it's our network IP address, not our Gateway IP address
