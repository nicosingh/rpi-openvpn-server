FROM resin/rpi-raspbian:stretch-20180214

MAINTAINER Nicolas Singh <nicolas.singh@gmail.com>

# Download required software
RUN apt-get -y update && \
  apt-get -y install openvpn easy-rsa iptables && \
  rm -rf /var/lib/apt/lists/*

# Copy server configuration files
COPY etc/openvpn/server.conf /etc/openvpn
COPY etc/sysctl.conf /etc/sysctl.conf

# Expose Keys volume
VOLUME /etc/openvpn/easy-rsa/keys

# Expose UDP port
EXPOSE 1194/udp

# Apply configurations and start OpenVPN server
CMD iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE &&\
    iptables-save > /etc/network/iptables.up.rules &&\
    bash -c "echo 'y' | iptables-apply" &&\
    OVPN_SERVER_IP=$(ip addr | grep eth0 | grep inet | awk '{print $2}' | awk -F"/" '{print $1}') &&\
    sed -i "s/OVPN_SERVER_IP/`echo $OVPN_SERVER_IP`/g" /etc/openvpn/server.conf &&\
    sed -i "s/OVPN_LOCAL_NETWORK/`echo $OVPN_LOCAL_NETWORK`/g" /etc/openvpn/server.conf &&\
    /etc/init.d/openvpn restart &&\
    sleep 5 &&\
    tail -f /var/log/openvpn.log
